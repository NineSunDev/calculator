import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { DisplayComponent } from './components/display/display.component';
import { KeypadComponent } from './components/keypad/keypad.component';
import { MainViewComponent } from './components/main-view/main-view.component';
import { TimesPipe } from './pipes/times/times.pipe';
import { OperationService } from './services/operation/operation.service';

@NgModule({
  bootstrap: [MainViewComponent],
  declarations: [
    DisplayComponent,
    KeypadComponent,
    MainViewComponent,
    TimesPipe,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
  ],
  providers: [
    OperationService
  ]
})
export class AppModule {
}
