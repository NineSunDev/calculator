import { NgModule } from '@angular/core';

import {
  RouterModule,
  Routes
} from '@angular/router';
import { MainViewComponent } from './components/main-view/main-view.component';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MainViewComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
