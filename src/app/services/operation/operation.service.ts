import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export interface OperationState {
  operation: string;
  isResult: boolean;
}
const defaultOperationState = {operation: '', isResult: false};

@Injectable()
export class OperationService {

  private operationState: BehaviorSubject<OperationState> = new BehaviorSubject<OperationState>(defaultOperationState);

  constructor() { }

  public setOperation(state: OperationState) {
    this.operationState.next(state);
  }

  public getOperation(): BehaviorSubject<OperationState> {
    return this.operationState;
  }

  public calculate() {
    this.operationState.next({
      operation: eval(this.operationState.value.operation),
      isResult: true
    });
  }
}
