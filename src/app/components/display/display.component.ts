import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { OperationService } from '../../services/operation/operation.service';

@Component({
  selector: 'display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayComponent implements OnInit {

  public displayText: string = '';

  constructor(
    private operationService: OperationService
  ) {
    this.operationService.getOperation().subscribe((state) => {
      this.displayText = state.operation;
    })
  }

  ngOnInit() {
  }
}
