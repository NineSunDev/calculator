import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { OperationService } from '../../services/operation/operation.service';

@Component({
  selector: 'keypad',
  templateUrl: './keypad.component.html',
  styleUrls: ['./keypad.component.scss'],
})
export class KeypadComponent implements OnInit {

  @Input() mode: string;

  public buttonMatrix: string[][] = simpleButtonMatrix;

  constructor(private opService: OperationService) {
  }

  ngOnInit() {
  }

  addToOperation(event: MouseEvent) {
    event.preventDefault();
    const target: HTMLElement = event.target as HTMLElement;
    const state               = this.opService.getOperation().value;

    if (target.innerHTML === '=') {
      this.opService.calculate();
    } else {
      const operation = state.isResult ? target.innerHTML : state.operation + target.innerHTML;
      this.opService.setOperation({operation, isResult: false});
    }

    event.stopPropagation();
  }
}

const simpleButtonMatrix: string[][] = [
  ['7', '8', '9', '(', ')'],
  ['4', '5', '6', '*', '/'],
  ['1', '2', '3', '+', '-'],
  ['0', '.', '',   '', '='],
];
